import CustomFormElementDelegating from "/emcJS/ui/element/CustomFormElementDelegating.js";
import {
    isEqual
} from "/emcJS/util/helper/Comparator.js";
import {
    debounce
} from "/emcJS/util/Debouncer.js";
import SimpleDataProvider from "/emcJS/util/dataprovider/SimpleDataProvider.js";
import ModalDialog from "/emcJS/ui/modal/ModalDialog.js";
import "/emcJS/ui/form/element/input/search/SearchInput.js";
import "/emcJS/ui/dataview/datagrid/DataGrid.js";
import TPL from "./PropertiesListInput.js.html" assert {type: "html"};
import STYLE from "./PropertiesListInput.js.css" assert {type: "css"};

// TODO add readonly mode

export default class PropertiesListInput extends CustomFormElementDelegating {

    #value;

    #searchEl;

    #gridEl;

    #jumpUnsetEl;

    #importEl;

    #addEl;

    #dataManager;

    constructor() {
        super();
        this.shadowRoot.append(TPL.generate());
        STYLE.apply(this.shadowRoot);
        /* --- */
        this.addEventListener("keydown", () => {
            // TODO
        });
        this.addEventListener("blur", (event) => {
            // TODO
            event.stopPropagation();
        });
        /* --- */
        this.#gridEl = this.shadowRoot.getElementById("grid");
        this.#gridEl.addEventListener("action::delete", (event) => {
            event.stopPropagation();
            event.preventDefault();
            const {rowKey} = event.data;
            const currentValue = {...this.#value};
            if (rowKey in currentValue) {
                delete currentValue[rowKey];
                this.value = currentValue;
            }
        });
        this.#gridEl.addEventListener("edit::value", debounce((event) => {
            event.stopPropagation();
            event.preventDefault();
            const {value, rowKey} = event.data;
            const currentValue = {...this.#value};
            if (rowKey in currentValue) {
                currentValue[rowKey] = value;
            }
            this.value = currentValue;
        }, 300));
        /* --- */
        this.#addEl = this.shadowRoot.getElementById("add");
        this.#addEl.addEventListener("click", async () => {
            let rowKey = null;
            const currentValue = this.value ?? {};
            while (rowKey == null) {
                rowKey = await ModalDialog.prompt("Add item", "Please enter a new key");
                if (typeof rowKey !== "string") {
                    return;
                }
                if (rowKey in currentValue) {
                    await ModalDialog.alert("Key already exists", `The key "${rowKey}" does already exist. Please enter another one!`);
                    rowKey = null;
                }
            }
            this.value = {
                ...currentValue,
                [rowKey]: ""
            };
        });
        /* --- */
        this.#importEl = this.shadowRoot.getElementById("import");
        this.#importEl.addEventListener("click", async () => {
            // TODO
        });
        /* --- */
        this.#jumpUnsetEl = this.shadowRoot.getElementById("jump-unset");
        this.#jumpUnsetEl.addEventListener("click", async () => {
            for (const key in this.#value) {
                const value = this.#value[key];
                if (typeof value !== "string" || value === "") {
                    const cellEl = this.#gridEl.getCell(key, "value");
                    cellEl.focus();
                    return;
                }
            }
        });
        /* --- */
        this.#dataManager = new SimpleDataProvider(this.#gridEl);
        this.#dataManager.setOptions({
            sort: ["name"]
        });
        /* --- */
        this.#searchEl = this.shadowRoot.getElementById("search");
        this.#searchEl.addEventListener("change", () => {
            const options = {filter: {}};
            if (this.#searchEl.value != "") {
                options.filter = {
                    name: this.#searchEl.value
                };
            }
            this.#dataManager.updateOptions(options);
        }, true);
    }

    connectedCallback() {
        super.connectedCallback();
        const value = this.value;
        this.#value = value;
        this.#applyValue(value ?? {});
        this.#applyRemainig(value ?? {});
        this.internals.setFormValue(value);
        this.#updateSort(this.sorted);
    }

    formDisabledCallback(disabled) {
        super.formDisabledCallback(disabled);
        this.#searchEl.disabled = disabled;
        this.#addEl.disabled = disabled;
        // TODO disable grid
    }

    formResetCallback() {
        this.value = super.value || "";
    }

    formStateRestoreCallback(state/* , mode */) {
        this.value = state;
    }

    set value(value) {
        if (!isEqual(this.#value, value)) {
            this.#value = value;
            this.#applyValue(value ?? {});
            this.#applyRemainig(value ?? {});
            this.internals.setFormValue(value);
            /* --- */
            this.dispatchEvent(new Event("change"));
        }
    }

    get value() {
        return this.#value ?? super.value;
    }

    set readonly(value) {
        this.setBooleanAttribute("readonly", value);
    }

    get readonly() {
        return this.getBooleanAttribute("readonly");
    }

    set sorted(value) {
        this.setBooleanAttribute("sorted", value);
    }

    get sorted() {
        return this.getBooleanAttribute("sorted");
    }

    static get observedAttributes() {
        return ["value", "readonly", "sorted"];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case "value": {
                if (oldValue != newValue) {
                    if (this.#value == null) {
                        try {
                            const value = JSON.parse(newValue);
                            this.#applyValue(value);
                            this.#applyRemainig(value);
                        } catch {
                            this.#applyValue({});
                            this.#jumpUnsetEl.style.display = "none";
                            this.#jumpUnsetEl.text = "";
                        }
                    }
                }
            } break;
            case "readonly": {
                if (oldValue != newValue) {
                    // TODO make everything readonly
                }
            } break;
            case "sorted": {
                if (oldValue != newValue) {
                    this.#updateSort(this.sorted);
                }
            } break;
        }
    }

    #updateSort(value) {
        if (value) {
            this.#dataManager.setOptions({
                sort: ["name"]
            });
        } else {
            this.#dataManager.setOptions({
                sort: []
            });
        }
    }

    #applyValue(value) {
        const data = Object.entries(value).map((row) => {
            return {
                key: row[0],
                name: row[0],
                value: row[1]
            };
        });
        this.#dataManager.setSource(data);
    }

    #applyRemainig(value) {
        let remaining = 0;
        for (const v of value) {
            if (typeof v !== "string" || v === "") {
                remaining++;
            }
        }
        if (remaining > 0) {
            this.#jumpUnsetEl.style.display = "";
            this.#jumpUnsetEl.text = `jump to next untranslated, ${remaining} remaining`;
        } else {
            this.#jumpUnsetEl.style.display = "none";
            this.#jumpUnsetEl.text = "";
        }
    }

}

customElements.define("jse-properties-input-list", PropertiesListInput);
