import CustomElement from "/emcJS/ui/element/CustomElement.js";
import "/emcJS/ui/form/FormContainer.js";
import "/emcJS/ui/form/field/DefaultFormFieldsLoader.js";
import "/emcJS/ui/i18n/I18nLabel.js";
import "../../ui/grid/DataGridTypeEntries.js";
import TPL from "./EditorForm.js.html" assert {type: "html"};
import STYLE from "./EditorForm.js.css" assert {type: "css"};

export default class EditorForm_Areas extends CustomElement {

    constructor() {
        super();
        this.shadowRoot.append(TPL.generate());
        STYLE.apply(this.shadowRoot);
        /* --- */
        const typeGridEl = this.shadowRoot.getElementById("type-grid");
        typeGridEl.addEventListener("edit", (event) => {
            event.stopPropagation();
            event.preventDefault();
            const {entityType, entityName} = event.data;
            const ev = new Event("edit", {bubbles: true});
            ev.data = {
                entityType,
                entityName
            };
            this.dispatchEvent(ev);
        });
    }

    reset() {
        // ignore
    }

    setName() {
        // ignore
    }

    setData() {
        // ignore
    }

    setFilterConfig() {
        // ignore
    }

    resetScroll() {
        // ignore
    }

}

customElements.define("gt-edt-world-form-areas", EditorForm_Areas);
