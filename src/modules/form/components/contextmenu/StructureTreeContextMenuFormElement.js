import ContextMenu from "/emcJS/ui/overlay/ctxmenu/ContextMenu.js";

export default class StructureTreeContextMenuFormElement extends ContextMenu {

    initItems() {
        super.setItems([
            {menuAction: "edit", content: "Edit"},
            // "splitter",
            // {menuAction: "moveup", content: "Move element up"},
            // {menuAction: "movedown", content: "Move element down"},
            "splitter",
            {menuAction: "addbefore", content: "Add element before"},
            {menuAction: "addafter", content: "Add element after"},
            "splitter",
            {menuAction: "remove", content: "Remove"}
        ]);
    }

    setItems() {
        // nothing
    }

}

customElements.define("jse-ctxmenu-edit-form-tree-form-element", StructureTreeContextMenuFormElement);
